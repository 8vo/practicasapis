function cargarDatos() {

  limpiar();

  let indice;
  if (document.getElementById("txtID").value != "")
    indice = document.getElementById("txtID").value;
  else alert("Ingresa un ID a buscar.");

  const http = new XMLHttpRequest();
  const url = "https://jsonplaceholder.typicode.com/albums/" + indice;

  // Realizar función de respuesta de peticións
  http.onreadystatechange = function () {
    // Validar respuesta del servidor
    if (this.status == 200 && this.readyState == 4) {
      let res = document.getElementById("lista");
      const json = JSON.parse(this.responseText);

      res.innerHTML +=
        '<tr> <td class="efecto">' +
        json.userId +
        " </td>" +
        ' <td class="efecto">' +
        json.id +
        " </td>" +
        ' <td class="efecto">' +
        json.title +
        " </td> </tr> ";

        
    } //else alert("Srugió un Error al hacer la petición");
  };

  http.open("GET", url, true);
  http.send();
}

function limpiar() {
  let res = document.getElementById("lista");

  res.innerHTML = "";
}


//Codificar los botones

document.getElementById("btnCargar").addEventListener("click", cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnIndex").addEventListener("click", function () {
  window.location.href = "../index.html";
});
