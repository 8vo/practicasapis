function cargarDatos() {
  const http = new XMLHttpRequest();
  const url = "https://jsonplaceholder.typicode.com/users";

  // Realizar función de respuesta de petición
  http.onreadystatechange = function () {
    limpiar();

    // Validar respuesta del servidor
    if (this.status == 200 && this.readyState == 4) {
      let usuario = document.getElementById("listaUsuario");
      let direccion = document.getElementById("listaDireccion");
      let empresa = document.getElementById("listaEmpresa");
      const json = JSON.parse(this.responseText);

      //Ciclo para mostrar los datos en la tabla
      for (const datos of json) {
        usuario.innerHTML +=
          "<tr> <td>" +
          datos.id +
          "</td>" +
          " <td>" +
          datos.name +
          " </td>" +
          " <td>" +
          datos.username +
          " </td>" +
          " <td>" +
          datos.email +
          " </td>" +
          " <td>" +
          datos.phone +
          " </td>" +
          " <td>" +
          datos.website +
          " </td>";
      }

      for (const datos of json) {
        direccion.innerHTML +=
          "<tr> <td>" +
          datos.id +
          "</td>" +
          " <td>" +
          datos.address.street +
          " </ td>" +
          " <td>" +
          datos.address.suite +
          " </ td>" +
          " <td>" +
          datos.address.city +
          " </ td>" +
          " <td>" +
          datos.address.zipcode +
          " </ td>" +
          " <td>" +
          datos.address.geo.lat +
          " </ td>" +
          " <td>" +
          datos.address.geo.lng +
          " </td> </tr> ";
      }

      for (const datos of json) {
        empresa.innerHTML +=
          "<tr> <td>" +
          datos.id +
          "</td>" +
          " <td>" +
          datos.company.name +
          " </td>" +
          " <td>" +
          datos.company.catchPhrase +
          " </td>" +
          " <td>" +
          datos.company.bs +
          " </td> </tr> ";
      }
    } //else alert("Srugió un Error al hacer la petición");
  };

  http.open("GET", url, true);
  http.send();
}

function limpiar() {
  let usuario = document.getElementById("listaUsuario");
  let direccion = document.getElementById("listaDireccion");
  let empresa = document.getElementById("listaEmpresa");

  usuario.innerHTML = "";
  direccion.innerHTML = "";
  empresa.innerHTML = "";
}

//Codificar los botones

document.getElementById("btnCargar").addEventListener("click", cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnIndex").addEventListener("click", function () {
  window.location.href = "../index.html";
});
