function mostrarDatos() {
  const nombrePaisInput = document.getElementById("nombrePais");
  const nombrePais = nombrePaisInput.value;

  if (nombrePais.trim() !== "") {
    fetch(`https://restcountries.com/v3.1/name/${nombrePais}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `No se pudo obtener la información del país. Estado: ${response.status}`
          );
        }
        return response.json();
      })
      .then((data) => {
        // Verificar si se encontró el país
        if (data.length > 0) {
          limpiar();
          const countryInfo = data[0];
          const capital = countryInfo.capital || "No disponible";
          const lenguaje = obtenerLenguajes(countryInfo);
          var ContenedorImagen = document.getElementById("bandera");
          let banderaUrl = " ";

          // Mostrar la información en el HTML
          document.getElementById("capital").value = capital;
          document.getElementById("lenguaje").value = lenguaje;

          console.log(banderaUrl);
          banderaUrl = countryInfo.flags.png;

          ContenedorImagen.innerHTML +=
            '<img src="' + banderaUrl + '" alt="Bandera">';
        } else {
          alert("País no encontrado");
        }
      })
      .catch((error) => {
        console.error("Error al obtener la información del país:", error);
      });
  } else {
    alert("Por favor, ingresa el nombre del país");
  }
}

function obtenerLenguajes(countryInfo) {
  if (countryInfo.languages) {
    if (Array.isArray(countryInfo.languages)) {
      return countryInfo.languages
        .map((lang) => lang.name || "No disponible")
        .join(", ");
    } else if (typeof countryInfo.languages === "object") {
      return Object.values(countryInfo.languages).join(", ");
    }
  }
  return "No disponible";
}

function limpiar() {
  document.getElementById("nombrePais").value = "";
  document.getElementById("capital").value = "";
  document.getElementById("lenguaje").value = "";

  var ContenedorImagen = document.getElementById("bandera");
  ContenedorImagen.innerHTML = " ";
}

function regresar() {
  window.location.href = "../index.html";
}

