const llamandoFetch = () => {
  const url = "https://dog.ceo/api/breeds/list";
  fetch(url)
    .then((response) => response.json())
    .then((data) => mostrarLista(data))
    .catch((error) => {
      const lblError = document.getElementById("lnlError");
      lblError.innerHTML = "Ocurrió un error 1: " + error;
    });
};

const llamandoFetchImagen = (raza) => {
  const url = "https://dog.ceo/api/breed/" + raza + "/images/random";

  fetch(url)
    .then((response) => response.json())
    .then((data) => verImagen(data))
    .catch((error) => {
      const lblError = document.getElementById("lnlError");
      lblError.innerHTML = "Ocurrió un error 2: " + error;
    });
};

const mostrarLista = (data) => {
  let lista = document.getElementById("lista");
  for (let index = 0; index < data.message.length; index++) {
    lista.innerHTML +=
      '<option value="' +
      data.message[index] +
      '">' +
      data.message[index] +
      "</option>";
  }
};

const verImagen = (data) => {
  const imagen = document.getElementById("imagen");
  imagen.src = " ";

  imagen.src = data.message;

  console.log(data.message);
};

function limpiar() {
  var imagen = document.getElementById("imagen");
  var lista = document.getElementById("lista");

  lista.innerHTML = " ";
  imagen.src = " ";

}

document.getElementById("btnLista").addEventListener("click", () => {
  llamandoFetch();
});
document.getElementById("btnImagen").addEventListener("click", () => {
  let raza = document.getElementById("lista").value;
  llamandoFetchImagen(raza);
});
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnIndex").addEventListener("click", function () {
  window.location.href = "../index.html";
});
