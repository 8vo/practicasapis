const llamandoFetch = () => {
  const url = "https://jsonplaceholder.typicode.com/todos";
  fetch(url)
    .then((response) => response.json())
    .then((data) => mostrarDatos(data))
    .catch((error) => {
      const lblError = document.getElementById("lnlError");
      lblError.innerHTML = "Ocurrió un error: " + error;
    });
};

// USANDO AWAIT
const llamdoAwait = async () => {
  try {
    const url = "https://jsonplaceholder.typicode.com/todos";
    const respuesta = await fetch(url);
    const data = await respuesta.json();

    mostrarDatos(data);
  } catch (error) {
    const lblError = document.getElementById("lnlError");
    lblError.innerHTML = "Ocurrió un error: " + error;
  }
};

// USANDO AXIOS
const llamandoAxios = async () => {
  const url = "https://jsonplaceholder.typicode.com/todos";

  axios
    .get(url)
    .then((res) => {
      mostrarDatos(res.data);
    })
    .catch((err) => {
      console.log("Surgió un error" + err);
    });
};

const mostrarDatos = (data) => {
  const res = document.getElementById("respuesta");
  res.innerHTML = "";

  for (let item of data) {
    res.innerHTML +=
      item.userId +
      " " +
      item.id +
      " " +
      item.title +
      " " +
      item.completed +
      "<br>";
  }
};

function limpiar() {
  let res = document.getElementById("respuesta");

  res.innerHTML = "";
}

document.getElementById("btnCargarP").addEventListener("click", () => {
  llamandoFetch();
});

document.getElementById("btnCargarA").addEventListener("click", () => {
  llamdoAwait();
});

document.getElementById("btnCargarAx").addEventListener("click", () => {
    llamandoAxios();
  });

document.getElementById("btnLimpiar").addEventListener("click", limpiar);
