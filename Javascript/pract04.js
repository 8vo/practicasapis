function cargarDatos() {
  limpiar();

  let indice;
  if (document.getElementById("txtID").value != "")
    indice = document.getElementById("txtID").value;
  else alert("Ingresa un ID a buscar.");

  const http = new XMLHttpRequest();
  const url = "https://jsonplaceholder.typicode.com/users/"+indice;

  // Realizar función de respuesta de petición
  http.onreadystatechange = function () {
    // Validar respuesta del servidor
    if (this.status == 200 && this.readyState == 4) {
      let usuario = document.getElementById("listaUsuario");
      let direccion = document.getElementById("listaDireccion");
      let empresa = document.getElementById("listaEmpresa");
      const json = JSON.parse(this.responseText);

      //Ciclo para mostrar los datos en la tabla
      // Datos de usuario
      usuario.innerHTML +=
        "<tr> <td>" +
        json.id +
        "</td>" +
        " <td>" +
        json.name +
        " </td>" +
        " <td>" +
        json.username +
        " </td>" +
        " <td>" +
        json.email +
        " </td>" +
        " <td>" +
        json.phone +
        " </td>" +
        " <td>" +
        json.website +
        " </td>";

      // Datos de la dirección
      direccion.innerHTML +=
        "<tr> <td>" +
        json.id +
        "</td>" +
        " <td>" +
        json.address.street +
        " </ td>" +
        " <td>" +
        json.address.suite +
        " </ td>" +
        " <td>" +
        json.address.city +
        " </ td>" +
        " <td>" +
        json.address.zipcode +
        " </ td>" +
        " <td>" +
        json.address.geo.lat +
        " </ td>" +
        " <td>" +
        json.address.geo.lng +
        " </td> </tr> ";

      // Datos de la empresa
      empresa.innerHTML +=
        "<tr> <td>" +
        json.id +
        "</td>" +
        " <td>" +
        json.company.name +
        " </td>" +
        " <td>" +
        json.company.catchPhrase +
        " </td>" +
        " <td>" +
        json.company.bs +
        " </td> </tr> ";
    } //else alert("Srugió un Error al hacer la petición");
  };

  http.open("GET", url, true);
  http.send();
}

function limpiar() {
  let usuario = document.getElementById("listaUsuario");
  let direccion = document.getElementById("listaDireccion");
  let empresa = document.getElementById("listaEmpresa");

  usuario.innerHTML = "";
  direccion.innerHTML = "";
  empresa.innerHTML = "";
}

//Codificar los botones

document.getElementById("btnCargar").addEventListener("click", cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnIndex").addEventListener("click", function () {
  window.location.href = "../index.html";
});
