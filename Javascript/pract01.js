function cargarDatos(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums"
    
    
    // Realizar función de respuesta de petición
    http.onreadystatechange = function(){
        // Validar respuesta del servidor
        if(this.status == 200 && this.readyState == 4){
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            //Ciclo para mostrar los datos en la tabla
            for(const datos of json){
                res.innerHTML+= '<tr> <td class="columna1">' + datos.userId + ' </td>'
                + ' <td class="columna2">' + datos.id + ' </td>'
                + ' <td class="columna3">' + datos.title + ' </td> </tr> '
            }
        } //else alert("Srugió un Error al hacer la petición");
    }

    http.open('GET', url, true);
    http.send();
}

//Codificar los botones

document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click', function(){
    let res = document.getElementById('lista');

    res.innerHTML="";
});
document.getElementById("btnIndex").addEventListener("click", function(){
    window.location.href="../index.html";
  })