// USANDO AXIOS
const llamandoAxios = async (id) => {
  const url = "https://jsonplaceholder.typicode.com/users/" + id;

  console.log(url);

  axios
    .get(url)
    .then((res) => {
      mostrarDatos(res.data);
    })
    .catch((err) => {
      console.log("Surgió un error" + err);
    });
};

const mostrarDatos = (data) => {
  var nombre = document.getElementById("txtNombre");
  var usuario = document.getElementById("txtUsuario");
  var correo = document.getElementById("txtCorreo");
  var direccion = document.getElementById("txtDireccion");

  // Mostrar los datos en los campos de entrada
  nombre.value = data.name;
  usuario.value = data.username;
  correo.value = data.email;
  direccion.value = data.address.street+" "+data.address.suite+" "+data.address.city+" "+data.address.zipcode;
};

function limpiar() {
  var nombre = document.getElementById("txtNombre");
  var usuario = document.getElementById("txtUsuario");
  var correo = document.getElementById("txtCorreo");
  var direccion = document.getElementById("txtDireccion");

  // Mostrar los datos en los campos de entrada
  nombre.value = " ";
  usuario.value = " ";
  correo.value = " ";
  direccion.value = " ";
}

document.getElementById("btnBuscar").addEventListener("click", () => {
  let id = document.getElementById("txtID").value;

  //    console.log(id);
  llamandoAxios(id);
});
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnIndex").addEventListener("click", function () {
  window.location.href = "../index.html";
});